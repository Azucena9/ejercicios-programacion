#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

int main() {
	int i;
	printf("Son numeros pares:\n");
	for (i=0;i<101;i++){
		if ((i%2) == 0){
			printf(" %i\n",i);
		}
	}
	return 0;
}