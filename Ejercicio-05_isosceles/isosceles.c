#include <stdio.h>

int main() {
	int  tamanoLadoIg,perimetro,tamanoLadoDif;
	printf("Introduzca el tamano de uno de los lados que son iguales\n");
	scanf("%i",&tamanoLadoIg);
	printf("Introduzca el tamano del lado que es diferente\n");
	scanf("%i",&tamanoLadoDif);
	perimetro = tamanoLadoIg*2 + tamanoLadoDif;
	printf("El perimetro es %i\n",perimetro);
	return 0;
}